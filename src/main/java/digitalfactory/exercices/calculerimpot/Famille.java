package digitalfactory.exercices.calculerimpot;

import java.util.ArrayList;
import java.util.List;

public class Famille {

    private final List<Membre> membres;

    public Famille() {
        this.membres = new ArrayList<>();
    }

    public void ajouter(final Membre membre) {
        this.membres.add(membre);
    }

    public List<Membre> getMembres() {
        return this.membres;
    }
}
