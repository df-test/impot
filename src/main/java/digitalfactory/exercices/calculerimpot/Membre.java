package digitalfactory.exercices.calculerimpot;

import java.math.BigDecimal;

public class Membre {

    private final Type type;
    private final BigDecimal revenue;

    public Membre(final Type type, final BigDecimal revenue) {
        this.type = type;
        this.revenue = revenue;
    }

    public Type getType() {
        return this.type;
    }

    public BigDecimal getRevenue() {
        return this.revenue;
    }
}
