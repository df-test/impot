package calculerimpot;

import digitalfactory.exercices.calculerimpot.Famille;
import digitalfactory.exercices.calculerimpot.Membre;
import digitalfactory.exercices.calculerimpot.Service;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static digitalfactory.exercices.calculerimpot.Type.ADULTE;
import static digitalfactory.exercices.calculerimpot.Type.ENFANT;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ServiceTest {

    private final Service service = new Service();

    @Test
    public void computeForSingle() {
        final Famille famille = new Famille();
        famille.ajouter(new Membre(ADULTE, new BigDecimal("32000")));
        assertEquals(0, new BigDecimal("3893.26").compareTo(this.service.calculer(famille)));
    }

    @Test
    public void computeForCoupleAndTwoChildren() {
        final Famille famille = new Famille();
        famille.ajouter(new Membre(ADULTE, new BigDecimal("32000")));
        famille.ajouter(new Membre(ADULTE, new BigDecimal("23950")));
        famille.ajouter(new Membre(ENFANT, null));
        famille.ajouter(new Membre(ENFANT, null));
        assertEquals(0, new BigDecimal("3714.06").compareTo(this.service.calculer(famille)));
    }

}
